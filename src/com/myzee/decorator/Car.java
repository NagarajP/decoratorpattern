package com.myzee.decorator;

public interface Car {
	public void assemble();
}

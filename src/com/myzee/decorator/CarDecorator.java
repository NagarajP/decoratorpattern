package com.myzee.decorator;

public class CarDecorator implements Car {

	Car car;
	public CarDecorator(Car car) {
		// TODO Auto-generated constructor stub
		this.car = car;
	}
	
	@Override
	public void assemble() {
		// TODO Auto-generated method stub
		this.car.assemble();
	}

}

package com.myzee.decorator;

public class DriveTheCar {

	public static void main(String[] args) {
		System.out.println("======Building Sports Car======");
		Car sportsCar = new SportsCar(new BasicCar());
		sportsCar.assemble();
		
		System.out.println("\n======Building Sports cum Luxury Car======");
		Car sportsLuxuryCar = new LuxuryCar(new SportsCar(new BasicCar()));
		sportsLuxuryCar.assemble();
	}

}
